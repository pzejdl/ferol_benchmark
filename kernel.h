/*
 * kernel.h
 *
 * Various GCC IO access and memory access declarations.
 *
 * By Petr Zejdl
 *
 */


#ifndef _KERNEL_H_
#define _KERNEL_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#if __GNUC__ < 3
# error Sorry, your compiler is too old/not recognized.
#endif

/* Optimization barrier */
#define barrier() __asm__ __volatile__("": : :"memory")

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

/*
 * Enable Some optimalization
 */

#if __GNUC__ >= 3
# define inline         inline __attribute__ ((always_inline))
# define __pure         __attribute__ ((pure))
//# define __const      __attribute__ ((const))
# define __noreturn     __attribute__ ((noreturn))
# define __malloc       __attribute__ ((malloc))
# define __must_check   __attribute__ ((warn_unused_result))
# define __deprecated   __attribute__ ((deprecated))
# define __used         __attribute__ ((used))
# define __unused       __attribute__ ((unused))
# define __packed       __attribute__ ((packed))
# define likely(x)      __builtin_expect (!!(x), 1)
# define unlikely(x)    __builtin_expect (!!(x), 0)
# define __constructor  __attribute__((constructor))
# define __destructor   __attribute__((destructor))
#else
# define inline         /* no inline */
# define __pure         /* no pure */
# define __const        /* no const */
# define __noreturn     /* no noreturn */
# define __malloc       /* no malloc */
# define __must_check   /* no warn_unused_result */
# define __deprecated   /* no deprecated */
# define __used         /* no used */
# define __unused       /* no unused */
# define __packed       /* no packed */
# define likely(x)      (x)
# define unlikely(x)    (x)
#endif

#define __iomem
#define __force


struct pci_resource {
	unsigned int bus_number;
	unsigned int devfn;
	uint32_t vendor;
	uint32_t device;
	unsigned int irq;
	uint64_t bar_start[7];
	uint64_t bar_size[7];
};


static inline uint8_t in_8(const volatile void *addr)
{
	return *(const volatile uint8_t *) addr;
}

static inline uint16_t in_16(const volatile void *addr)
{
	return *(const volatile uint16_t *) addr;
}

static inline uint32_t in_32(const volatile void *addr)
{
	return *(const volatile uint32_t *) addr;
}

static inline void out_8(uint8_t b, volatile void *addr)
{
	*(volatile uint8_t *) addr = b;
}

static inline void out_16(uint16_t b, volatile void *addr)
{
	*(volatile uint16_t *) addr = b;
}

static inline void out_32(uint32_t b, volatile void *addr)
{
	*(volatile uint32_t *) addr = b;
}

static inline uint64_t in_64(const volatile void *addr)
{
	return *(const volatile uint64_t *) addr;
}

static inline void out_64(uint64_t b, volatile void *addr)
{
	*(volatile uint64_t *) addr = b;
}

int find_pci_resource(struct pci_resource *pci, uint32_t vendor_id, uint32_t device_id, uint32_t index);
void *ioremap(off_t phys_addr, size_t size);
int iounmap(volatile void *addr, size_t size);

#ifdef __cplusplus
}
#endif

#endif /* _KERNEL_H_ */

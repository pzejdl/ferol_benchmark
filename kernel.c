/*
 * kernel.c
 *
 * Memory mapping functions (similar to functions in kernel).
 *
 * By Petr Zejdl
 *
 */


#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "kernel.h"


/*
 * Find a PCI resource
 * Retuns: 0 on success, -1 otherwise
 */

int find_pci_resource(struct pci_resource *pci, uint32_t vendor_id, uint32_t device_id, uint32_t index)
{
	FILE *dev;	
	unsigned int count = 0;
	int found = -1;
	int i;
		
	if ((dev = fopen("/proc/bus/pci/devices", "r")) == NULL) {
		perror("Error during openning '/proc/bus/pci/devices'");
		return -1;
	}
	
	while (!feof(dev)) {
		if (fscanf(dev, "%02x%02x\t%04x%04x\t%x", &pci->bus_number, &pci->devfn, &pci->vendor, &pci->device, &pci->irq) != 5) {
			goto finished;
		}
		for (i=0; i<7; i++) {
			if (fscanf(dev, "\t%16lx", &pci->bar_start[i]) != 1) {
				perror("Error parsing PCI configuration");
				fclose(dev);
				return -1;
			}
		}
		for (i=0; i<7; i++) {
			if (fscanf(dev, "\t%16lx", &pci->bar_size[i]) != 1) {
				perror("Error parsing PCI configuration");
				fclose(dev);
				return -1;
			}
		}
		/* SKip the driver name */
		fscanf(dev, "%*[^\n\r]");
		//printf("%04x:%04x\n", pci->vendor, pci->device);
		if (pci->vendor == vendor_id && pci->device == device_id) {
			if (count == index) {
				found = 0;
				break;
			}
			count++;
		}
	}
finished:
	
	fclose(dev);
	return found;
}

/*
 * Map platfrom memory into CPU user space and returns virtual address
 */

// — Function: void * mmap (void *address, size_t length, int protect, int flags, int filedes, off_t offset)
void *ioremap(off_t phys_addr, size_t size)
{
	off_t offset, page_mask;
	void *addr;
	int fd;
	
//	DEBUG_CMD(
//		printf("phys_addr: %.8lX\n", phys_addr);
//	);

	page_mask = sysconf(_SC_PAGE_SIZE) -1;
	offset = phys_addr & page_mask;
	phys_addr = phys_addr & ~page_mask;
	
	if (unlikely((fd = open("/dev/mem", O_RDWR|O_SYNC)) < 0)) {
		perror("Error opening /dev/mem file");
		return NULL;
	}
		
//	printf("fd = %i\n", fd);
	
	if (unlikely((addr = mmap(0, size+(size_t)offset, PROT_READ|PROT_WRITE, MAP_SHARED, fd, phys_addr)) == MAP_FAILED)) {
		perror("MMAP error");
		close(fd);
		return NULL;
	}
	return (void *) (offset + (char *)addr);
}


/*
 * Unmap previously mapped memory
 */		       
int iounmap(volatile void *addr, size_t size)
{
 	off_t offset, page_mask;
	
	page_mask = sysconf(_SC_PAGE_SIZE) -1;
	offset = (off_t)addr & page_mask;
 
	return munmap((void*)addr-offset, size+(size_t)offset);
} 		       
       


/*
 * bench.cc
 * 
 * CMS DAQ I/O Benchmarking tool.
 * 
 * By Petr Zejdl
 *
 */

#if __GNUG__ < 8
#error "This software is written for C++17 and requires GCC 8"
#endif

#include <cassert>
#include <iostream>
#include <chrono>
#include <thread>
#include <sstream>
#include <iomanip>
#include <typeinfo>
#include <memory>
#include <deque>
#include <numeric>
#include <functional>

#include "hal/PCIAddressTable.hh"
#include "hal/PCIAddressTableASCIIReader.hh"
#include "hal/PCIDevice.hh"
#include "hal/PCILinuxBusAdapter.hh"

#include "kernel.h"

#define FEROLADDRESSTABLE   "FEROLAddressTable.dat"
#define FEROL_VENDORID      0xECD6 /**< needed to search for the device */
#define FEROL_DEVICEID      0xFEA0 /**< needed to search for the device */

/*
 * We are accessing 64K of QDR memory
 */
#define FEROL_MEMORY        "Memory_QDR"
#define FEROL_MEMORY_SIZE   65536
//#define FEROL_REGISTER      "IP_SOURCE"


/******************************************************************************
 * Memory Mapped Access
 * MMAPDevice class here is providing the same I/O access methods as HAL library
 *****************************************************************************/

class MMAPDevice {
public:
    // Physical address
    uint64_t address_;
    // MMAPed virtual address
    volatile void *vaddr_;
    // Size of the MMAPed address space
    size_t size_;

public:
    MMAPDevice(uint64_t address, size_t size) : address_(address), size_(size)
    {
        if ((vaddr_ = ioremap(address, size)) == NULL) {
            std::ostringstream ss;
            ss << "Cannot MMAP address: 0x" << std::hex << address;
            throw std::runtime_error(ss.str());
        }
    }

    // Disable copy constructor
    MMAPDevice(const MMAPDevice&) = delete;

    // Disable copy assignment operator
    MMAPDevice& operator=(const MMAPDevice&) = delete;

    ~MMAPDevice()
    {
        iounmap(vaddr_, size_);
    }

    // Prints some information about MMAPed area
    friend std::ostream& operator<< (std::ostream& os, const MMAPDevice& dev)
    {
        os
            << "MMAPDevice(address=0x" << std::hex << dev.address_
            << ", vaddr=0x" << reinterpret_cast<size_t>(dev.vaddr_) << std::dec
            << ", size=" << dev.size_ << ')';
        return os;
    }

    inline void unmaskedWrite( [[maybe_unused]] std::string item, uint32_t data, [[maybe_unused]] HAL::HalVerifyOption verifyFlag = HAL::HAL_NO_VERIFY, uint32_t offset = 0 ) const
    {
        out_32(data, reinterpret_cast<void *>( reinterpret_cast<size_t>(vaddr_) + offset) );
    }

    inline void unmaskedWrite64( [[maybe_unused]] std::string item, uint64_t data, [[maybe_unused]] HAL::HalVerifyOption verifyFlag = HAL::HAL_NO_VERIFY, uint64_t offset = 0 ) const
    {
        out_64(data, reinterpret_cast<void *>( reinterpret_cast<size_t>(vaddr_) + offset) );
    }

    inline void unmaskedRead( [[maybe_unused]] std::string item, uint32_t* result, uint32_t offset = 0 ) const
    {
        *result = in_32( reinterpret_cast<void *>( reinterpret_cast<size_t>(vaddr_) + offset) );
    }

    inline void unmaskedRead64( [[maybe_unused]] std::string item, uint64_t* result, uint64_t offset = 0 ) const
    {
        *result = in_64( reinterpret_cast<void *>( reinterpret_cast<size_t>(vaddr_) + offset) );
    }
};


/******************************************************************************
 * Generic  PCIDevice class managing lifetime of HAL and MMAP related objects
 *****************************************************************************/

/*
 * PCIDevice class encapsulating everything that is necessary to keep alive
 * for accessing PCI device over HAL and MMAP.
 */
struct PCIDevice {
    // HAL Related stuff
    HAL::PCIAddressTableASCIIReader addressTableReader_;
    HAL::PCIAddressTable addressTable_;
    HAL::PCILinuxBusAdapter busAdapter_;
    HAL::PCIDevice halDevice_;

    //MMAP stuff
    uint64_t physicalAddress_;
    MMAPDevice mmapDevice_;

    PCIDevice(std::string addressTableFileName, uint32_t vendorId, uint32_t deviceId, uint32_t pciIndex)
        : addressTableReader_(addressTableFileName)
        , addressTable_("Test address table", addressTableReader_ )
        , busAdapter_()
        , halDevice_(addressTable_, busAdapter_, vendorId, deviceId, pciIndex)

        /*
         * OK - Hardcoding FEROL_MEMORY and FEROL_MEMORY_SIZE here is not nice. 
         * Much better would be to MMAP the whole PCI BAR and decide what we want to access later.
         * But for simplicity and my laziness it is like what it is, for the moment...
         */

        , physicalAddress_(halDevice_.getItemAddress(FEROL_MEMORY))
        , mmapDevice_(physicalAddress_, FEROL_MEMORY_SIZE)
    {
        uint32_t slot;
        halDevice_.read("Geographic_Address", &slot);

        std::cout
            << "FEROL[" << pciIndex << "]:"
            << " Slot: " << slot
            << ", QDR addr: 0x" << std::hex << std::setw(8) << std::setfill('0') << physicalAddress_ << '\n';
    }
};

// A vector of PCI Devices. Just an alias for convenience.
// Cannot use std::vector because PCIDevice is not CopyAssignable and not CopyConstructible.
using PCIDevices = std::deque<PCIDevice>;


/******************************************************************************
 * Benchmark functions
 *****************************************************************************/

template<typename Device>
void test_write32(Device& dev, size_t size)
{
    for (unsigned int m = 0; m < (size >> 16); m++) {
        for (unsigned int i = 0; i < 65536; i+= 4) {
            dev.unmaskedWrite(FEROL_MEMORY, i, HAL::HAL_NO_VERIFY, i);
        }
    }
}

template<typename Device>
void test_read32(const Device& dev, size_t size)
{
    uint32_t value;
    for (unsigned int m = 0; m < (size >> 16); m++) {
        for (unsigned int i = 0; i < 65536; i+= 4) {
            dev.unmaskedRead(FEROL_MEMORY, &value, i);
            assert( value == i );
        }
    }
}

template<typename Device>
void test_combined32(const Device& dev, size_t size)
{
    uint32_t value;
    for (unsigned int m = 0; m < (size >> 16); m++) {
        for (unsigned int i = 0; i < 65536; i+= 4) {
            dev.unmaskedWrite(FEROL_MEMORY, i, HAL::HAL_NO_VERIFY, i);
            dev.unmaskedRead(FEROL_MEMORY, &value, i);
            assert( value == i );
        }
    }
}

template<typename Device>
void test_write64(const Device& dev, size_t size)
{
    for (unsigned int m = 0; m < (size >> 16); m++) {
        for (unsigned int i = 0; i < 65536; i+= 8) {
            dev.unmaskedWrite64(FEROL_MEMORY, i, HAL::HAL_NO_VERIFY, i);
        }
    }
}

template<typename Device>
void test_read64(const Device& dev, size_t size)
{
    uint64_t value;
    for (unsigned int m = 0; m < (size >> 16); m++) {
        for (unsigned int i = 0; i < 65536; i+= 8) {
            dev.unmaskedRead64(FEROL_MEMORY, &value, i);
            assert( value == i );
        }
    }
}

template<typename Device>
void test_combined64(const Device& dev, size_t size)
{
    uint64_t value;
    for (unsigned int m = 0; m < (size >> 16); m++) {
        for (unsigned int i = 0; i < 65536; i+= 8) {
            dev.unmaskedWrite64(FEROL_MEMORY, i, HAL::HAL_NO_VERIFY, i);
            dev.unmaskedRead64(FEROL_MEMORY, &value, i);
            assert( value == i );
        }
    }
}


/******************************************************************************
 * Benchmark tools
 *****************************************************************************/

/*
 * This will time execution of function F and returns time in seconds.
 */
template<typename F, typename... Args>
double timeExecution(F func, Args&&... args)
{
    auto start = std::chrono::high_resolution_clock::now();
    func(std::forward<Args>(args)...);
    auto end = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double> diff = end - start;
    return diff.count();
}

/*
 * Calculates throughput in MB/s and returns it as a std::string.
 */
std::string getThroughput(size_t volume, double time)
{
    // in MB/sec
    double throughput = static_cast<double>(volume) / time / double{1<<20};

    std::ostringstream ss;
    ss << std::setw(8) << std::fixed << std::setprecision(4) << throughput << " MB/s";
    return ss.str();
}


/******************************************************************************
 * Benchmark management
 *****************************************************************************/

/*
 * Runs a function in multiple threads over collection of hardware devices. 
 */
template<typename Devices, typename Function>
void runSingleTest(std::string description, size_t testMemorySize, const Devices& devices, size_t nbThreads, Function function)
{
    // Where to collect results
    std::vector<double> collectedExecutionTimes(nbThreads);

    // Collection of threads
    std::vector<std::thread> threads;
    threads.reserve(nbThreads);

    // ------------------------------------------------------------------------
    // Spawn N= threads for concurent access (to different boards)
    for (size_t i = 0; i < nbThreads; i++) {
        auto& device = devices[i];
        double& executionTime = collectedExecutionTimes[i];

        // Use a lambda function to run the test in a thread
        threads.emplace_back([&device, &testMemorySize, &executionTime, &function]
        {
            executionTime = timeExecution( function, device, testMemorySize );
        });
    }
    // ------------------------------------------------------------------------

    // Wait for all threads to finish their job
    for (auto& t : threads) {
        t.join();
    }

    // Calculate average and print results
    double sum = std::accumulate(collectedExecutionTimes.begin(), collectedExecutionTimes.end(), 0.0);
    double avg = sum / static_cast<double>(nbThreads);

    std::cout << description << " throughput: " << getThroughput(testMemorySize, avg) << " [";
    for (auto result : collectedExecutionTimes) {
        std::cout << getThroughput(testMemorySize, result) << ' ';
    }
    std::cout << " ] time: " << std::setfill(' ') << std::setw(8) << std::fixed << std::setprecision(4) << avg << " s" << std::endl;
}

/*
 * Runs a scaling tests 
 */
template<typename Devices, typename Function>
void runScalingTest(std::string description, size_t testMemorySize, const Devices& devices, size_t nbThreads, bool runScalingTests, Function function)
{
    // for (size_t threads = (runScalingTests ? 1 : nbThreads); threads <= nbThreads; threads++) {
    //     runSingleTest(std::to_string(threads) + "x " + description, testMemorySize, devices, threads, function);
    // }
    if (runScalingTests && nbThreads > 1) {
        // In scaling mode we run with one thread and then with nbTreads
        runSingleTest("1x " + description, testMemorySize, devices, 1, function);
    }
    runSingleTest(std::to_string(nbThreads) + "x " + description, testMemorySize, devices, nbThreads, function);
}

/*
 * Run all tests
 */
template<typename Device, typename Devices>
void runTests(std::string description, size_t testMemorySize, size_t nbThreads, bool runScalingTests, const Devices& devices)
{
    // Writes are faster, we increase test size in order to equalize the test time
    runScalingTest(description + "write64 ", 10*testMemorySize, devices, nbThreads, runScalingTests, test_write64<Device>);
    runScalingTest(description + "read64  ",    testMemorySize, devices, nbThreads, runScalingTests, test_read64<Device>);
    runScalingTest(description + "combo64 ",    testMemorySize, devices, nbThreads, runScalingTests, test_combined64<Device>);
    runScalingTest(description + "write32 ", 10*testMemorySize, devices, nbThreads, runScalingTests, test_write32<Device>);
    runScalingTest(description + "read32  ",    testMemorySize, devices, nbThreads, runScalingTests, test_read32<Device>);
    runScalingTest(description + "combo32 ",    testMemorySize, devices, nbThreads, runScalingTests, test_combined32<Device>);
}


/******************************************************************************
 * Some additional functions, tests
 *****************************************************************************/

/*
 * Run a test over specified clock class and prints resolution.
 */
#define printClockResolution(Clock)     printClockResolution_<Clock>(#Clock)

template <typename Clock>
void printClockResolution_(std::string typeName)
{
    std::cout << "Testing clock: " << typeName << '\n';
    std::cout << "  Type precision: " << Clock::period::num << "/" << Clock::period::den << " second " << '\n';
    std::cout << "  Is steady?    : " << (Clock::is_steady ? "Yes" : "No") << std::endl;
    std::cout << "  Sleeping for 1 second...";

    auto start = Clock::now();
    std::this_thread::sleep_for( std::chrono::microseconds(1000000) );
    auto end = Clock::now();

    std::chrono::duration<double> diff = end - start;
    std::cout << ' ' << std::setw(11) << std::fixed << std::setprecision(9) << diff.count() << " seconds elapsed" << std::endl;
}

void runClockTest()
{
    printClockResolution(std::chrono::high_resolution_clock);
    std::cout << std::endl;
    printClockResolution(std::chrono::system_clock);
    std::cout << std::endl;
    printClockResolution(std::chrono::steady_clock);
}

/******************************************************************************
 * Main
 *****************************************************************************/

void printHelp(char *argv0)
{
    std::cout << "Usage: " << argv0 << " Size_in_MB [Threads] [[Scaling (0/1)]] \n";
}

int main(int argc, char *argv[])
{
    bool optionTestClocks = false;
    size_t optionPciIndex = 0;
    size_t optionTestMemorySize = 1;
    size_t optionNbThreads = 1;
    bool optionRunScalingTests = false;

    // Very simple argument processing...
    switch (argc-1) {
    case 3:
        optionRunScalingTests = (std::stoi( argv[3] ) ? true : false );
        [[fallthrough]];
    case 2:
        optionNbThreads = std::stoul( argv[2] );
        [[fallthrough]];
    case 1:
        optionTestMemorySize = std::stoul( argv[1] );
        break;
    default:
        printHelp(argv[0]);
        exit(-1);   
    }

    std::cout 
        << "Test memory size:     " << optionTestMemorySize << " MB\n"
        << "Threads to use:       " << optionNbThreads << '\n'
        << "Perfrom scaling test: " << (optionRunScalingTests ? "Yes" : "No") << std::endl;

    if (optionTestClocks) {
        runClockTest();
        return 0;
    }

    // Keep list of PCI devices, i.e. make sure that necessary objects don't go prematurely out of scope
    PCIDevices pciDevices;
    // Get list of HAL PCIDevices
    std::vector<std::reference_wrapper<const HAL::PCIDevice>> halDevices;
    // Get list of MMAP Devices
    std::vector<std::reference_wrapper<const MMAPDevice>> mmapDevices;

    // Opens multiple PCI devices later used in concurent read/write benchmark
    for (size_t pciIndex = optionPciIndex; pciIndex <= optionPciIndex + optionNbThreads - 1; pciIndex++) {
        //NOTE: Returning reference from emplace_back requires C++17
        PCIDevice& pciDevice = pciDevices.emplace_back( FEROLADDRESSTABLE, FEROL_VENDORID, FEROL_DEVICEID, pciIndex );
        halDevices.emplace_back( pciDevice.halDevice_ );
        mmapDevices.emplace_back( pciDevice.mmapDevice_ );
    }

    assert( pciDevices.size() == static_cast<size_t>(optionNbThreads) );

    // ------------------------------------------------------------------------

    optionTestMemorySize *= (1<<20);

    /******************************************************************************
     * MMAP Tests
     *****************************************************************************/

    std::cout << "MMAP Tests:" << std::endl;
    runTests<const MMAPDevice>("MMAP ", optionTestMemorySize, optionNbThreads, optionRunScalingTests, mmapDevices);

    /******************************************************************************
     * HAL Tests
     *****************************************************************************/

    std::cout << "HAL Tests:" << std::endl;
    runTests<const HAL::PCIDevice>("HAL ", optionTestMemorySize, optionNbThreads, optionRunScalingTests, halDevices);

    /*****************************************************************************/

    std::cout << "main() finished.\n";

    return 0;
}

# ferol_benchmark

Simple PCI I/O test with FEROL device.

It MMAPs the second memory BAR, where 64KB of QDR memory is located and runs WRITE/READ test.

## Build

This software requires [XDA15](http://www.cern.ch/xdaq).

#### Compile with
```
$ source ldpath
$ make
```

## Run

In order to read/write directly to the hardware, the software requires ROOT privileges.

```
$ sudo taskset -c 1,2,3,4 ./bench 100 4 1
```

One has to use correct CPU core numbers (depending on where the hardware is connected in NUMA system).

#### Usage:
```
Usage: ./bench Size_in_MB [Threads] [[Scaling (0/1)]] 

Parameters:
1:  Memory size in MB
2:  Number of threads to use
3:  0/1 (No, Yes) - Whether to perform a scaling test (communicate with multiple cards in parallel)

Example:
./bench 100 4 1
```

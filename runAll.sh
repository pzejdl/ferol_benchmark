#!/bin/sh
#DIR="/globalscratch/pzejdl/log-bench/2019-09-16"
TST=$(date "+%Y-%m-%d--%H-%M-%S")
DIR="/tmp/ferol_benchmark/$TST"

mkdir -p "$DIR"
LOG="$DIR/bench-$(hostname).log"

CMD="./run15.sh 100 4 1"

date > $LOG
echo "$CMD" >> $LOG
$CMD 2>&1 >> $LOG
date >> $LOG

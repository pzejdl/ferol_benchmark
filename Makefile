LIBS=-lGenericHAL -lPCILinuxBusAdapter -lxpci -lxcept -lxerces-c -lconfig -ltoolbox -llog4cplus -lasyncresolv -luuid -lpthread

#INCDIR=-I/opt/xdaq/include/hal -I/opt/xdaq/include
INCDIR=-I/opt/xdaq/include
LIBDIR=-L/opt/xdaq/lib

#CFLAGS+=-W -Wall -Wconversion -O3 -std=c++11 -g
CFLAGS+=-W -Wall -Wconversion -O3 -std=c++17 -g


# -ftree-vectorizer-verbose=5 -ftree-vectorize
#vpath %.cc :../../common
default: bench

kernel.o : kernel.c kernel.h
	$(CC) $(CFLAGS) -c $(INCDIR) $< -o $@

bench.o : bench.cc
	$(CXX) $(CFLAGS) -c $(INCDIR) $< -o $@
#	$(CXX) $(CFLAGS) -g -c -fverbose-asm -masm=intel -Wa,-ahl=bench.asm $(INCDIR) $< 

bench : bench.o kernel.o
	$(CXX) $(CFLAGS) $(LIBDIR) $(LIBS) kernel.o $< -o bench

ldpath :
	echo "export LD_LIBRARY_PATH=/opt/xdaq/lib" > ldpath

clean:
	rm -rf $(NAME) bench bench.o kernel.o
